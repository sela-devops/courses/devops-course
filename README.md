# DevOps Course

We offer to types of devops courses, one using jenkins and another one using azure devops.  
Both are just a set of other courses offered together as shown below:

---

### DevOps Course (Jenkins)

 - Day 1: [Git Basic](https://gitlab.com/sela-devops/courses/git-basic)
 - Day 2: [Jenkins CI/CD](https://gitlab.com/sela-devops/courses/jenkins-cicd)
 - Day 3: [Docker](https://gitlab.com/sela-devops/courses/docker)
 - Day 4: [Ansible](https://gitlab.com/sela-devops/courses/ansible)
 - Day 5: [Microservices Workshop (Jenkins)](https://gitlab.com/sela-devops/courses/jenkins-microservices)
 - Day 6: [Kubernetes](https://gitlab.com/sela-devops/courses/kubernetes)
 - Day 7: [Kubernetes](https://gitlab.com/sela-devops/courses/kubernetes)

---

### DevOps Course (Azure DevOps)

 - Day 1: [Git Basic](https://gitlab.com/sela-devops/courses/git-basic)
 - Day 2: [Azure DevOps CI/CD](https://gitlab.com/sela-devops/courses/azure-devops-cicd)
 - Day 3: [Docker](https://gitlab.com/sela-devops/courses/docker)
 - Day 4: [Ansible](https://gitlab.com/sela-devops/courses/ansible)
 - Day 5: [Microservices Workshop (Azure DevOps)](https://gitlab.com/sela-devops/courses/azure-devops-microservices)
 - Day 6: [Kubernetes](https://gitlab.com/sela-devops/courses/kubernetes)
 - Day 7: [Kubernetes](https://gitlab.com/sela-devops/courses/kubernetes)

---

For more details see the information repository in each course